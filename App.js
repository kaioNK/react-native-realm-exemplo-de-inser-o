import React, {Component} from 'react'
import {StyleSheet, Platform, View, Image, Text, TextInput, TouchableOpacity, Alert} from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    margin: 10
  },
  textInputStyle: {
    borderWidth: 1,
    borderColor: '#009688',
    width: '100%',
    height: 40,
    borderRadius: 10,
    marginBottom: 10,
    textAlign: 'center'
  },
  button: {
    width: '100%',
    height: 40,
    padding: 10,
    backgroundColor: '#4CAF50',
    borderRadius: 7,
    marginTop: 12
  },
  textStyle: {
    color: '#fff',
    textAlign: 'center'
  }

})

const Realm = require('realm')
let realm

class App extends Component{

  constructor(props){
    super(props)
    this.state = {
      student_name: '',
      student_class: '',
      student_subject: ''
    }

    realm = new Realm({
      schema: [{name: 'Student_Info', 
        properties: {
          student_id: {type: 'int', default: 0},
          student_name: 'string',
          student_class: 'string',
          student_subject: 'string'
        }
      }]
    })

  }//fim do construtor

  add_student = () => {
    realm.write(() => {
      let ID = realm.objects('Student_Info').length + 1
      realm.create('Student_Info', {
        student_id: ID,
        student_name: this.state.student_name, 
        student_class: this.state.student_class,
        student_subject: this.state.student_subject
      })
    })
    Alert.alert('Student details add successfully.')
  }

  render(){
    //variável que armazenará todos os objetos inseridos no BD, só para mostrar os dados inseridos na tela.
    let info = realm.objects('Student_Info')

    //precisamos converter os dados em JSON para poder acessá-la.
    let myJSON = JSON.stringify(info)
    return(
      <View style={styles.container}>
        <TextInput 
          placeholder="Enter Student Name"
          autoCorrect={false}
          style={styles.textInputStyle}
          underlineColorAndroid = 'transparent'
          onChangeText={(text) => {this.setState({student_name: text})}}
        />

        <TextInput 
          placeholder='Enter Student Class'
          style={styles.textInputStyle}
          underlineColorAndroid='transparent'
          onChangeText={(text) => {this.setState({student_class: text})}}
        />

        <TextInput 
          placeholder='Enter Student Object'
          style={styles.textInputStyle}
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setState({student_subject: text})}
        />

        <TouchableOpacity onPress={this.add_student} activeOpacity={0.7} style={styles.button}>
          <Text style={styles.textStyle}>CLICK HERE TO ADD STUDENT DETAILS</Text>
        </TouchableOpacity>

        <Text style={{marginTop: 10}}>{myJSON}</Text>
        
      </View>
    )
    
  }
}

export default App